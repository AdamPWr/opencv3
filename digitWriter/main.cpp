#include<iostream>
#include<opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main(int n,char** atgv)
{

    Mat mainBlock = Mat::zeros(500,500,CV_8UC1);

    char alphanum= 0;

    int xFactor = 0;
    int yFactor = 20;

    while(alphanum != 27)
    {
        namedWindow("W1",CV_WINDOW_AUTOSIZE);
        putText(mainBlock,string(1,alphanum),Point(xFactor,yFactor),FONT_HERSHEY_PLAIN,2,Scalar(100,100,255),2);

        imshow("W1",mainBlock);
        alphanum = waitKey(0);
        xFactor += 15;
        if(xFactor>=500)
        {
            yFactor+=20;
            xFactor = 0;
        }
    }

    return 0;
}