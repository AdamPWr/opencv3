#include<opencv2/opencv.hpp>
#include<iostream>
#include<error.h>

using namespace std;
using namespace cv;

int main()
{
    Mat img1,img2;

    namedWindow("W1",WINDOW_AUTOSIZE);
    namedWindow("W2",WINDOW_AUTOSIZE);

    img1 = imread("hof.jpeg");
    pyrUp(img1,img2);

    imshow("W1",img1);
    imshow("W2",img2);

    waitKey(0);

    return 0;
}