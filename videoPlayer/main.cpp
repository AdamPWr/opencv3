#include<opencv2/opencv.hpp>
#include<iostream>
#include<exception>

using namespace std;
using namespace cv;

void sliderCallback(int,void*);

VideoCapture cap;

int main(int args, char** argv)
{
    cap.open("../chap1/doggo.avi");

    if(!cap.isOpened())
    {
        throw(runtime_error("Nie otwarto filmu"));
    }

    namedWindow("Player",WINDOW_AUTOSIZE);
    int frames = int(cap.get(CV_CAP_PROP_FRAME_COUNT));
    auto slider_poz = 0;
    createTrackbar("Pozycja","Player",&slider_poz,frames,sliderCallback);
    Mat frame;
    for(;;)
    {   
         cap>>frame;
         if(frame.empty()) break;
         slider_poz = (int)cap.get(CV_CAP_PROP_POS_FRAMES);
         setTrackbarPos("Pozycja","Player",slider_poz);
         imshow("Player",frame);

         if(waitKey(30)>=0) break;
    }

    return 0;
}

void sliderCallback(int poz, void*)
{
    cap.set(CV_CAP_PROP_POS_FRAMES,poz);
}