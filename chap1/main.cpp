#include <opencv2/opencv.hpp>
#include <iostream>
#include<exception>
 
using namespace cv;
using namespace std;

void ex1(); //im show
void ex2(); //video cap

int main( int argc, char** argv )
{
    try
    {
         ex2();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    
   
}

void ex1()
{

    Mat img = imread("a1.jpg");

    if(img.empty()) 
    {

        throw(runtime_error("Nie otwarto obrazu"));
    }

    namedWindow("Example1",WINDOW_AUTOSIZE);
    imshow("Example",img);
    waitKey(0);

}

void ex2()
{
    VideoCapture cap;

    cap.open("doggo.avi");

    if(!cap.isOpened())
    {
        throw(runtime_error("Nie otwarto filmu"));
    }

    namedWindow("Example2", WINDOW_AUTOSIZE);
    Mat frame;
    for(;;)
    {
        cap>>frame;
        if(frame.empty()) break;
        imshow("Example2",frame);

        if(waitKey(33)>=0) break;
    }
}