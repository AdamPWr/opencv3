#include<opencv2/opencv.hpp>
#include<iostream>
#include<error.h>

using namespace std;
using namespace cv;

int main()
{   
    namedWindow("W1",WINDOW_AUTOSIZE);
    namedWindow("W2",WINDOW_AUTOSIZE);
    VideoCapture cap;
    cap.open(0);
    Mat img1,img2,img3;

    if(!cap.isOpened())
    {
        cerr<<"Nie otwarto kamery"<<endl;
    }


    for(;;)
    {

        cap>> img1;

        if(img1.empty())    break;

        cvtColor(img1,img2,COLOR_RGB2GRAY);
        Canny(img2,img3,10,100,3,true);

        imshow("W1",img1);
        imshow("W2",img3);
        if(waitKey(30)>0)   break;
    }

/*
    namedWindow("W3",WINDOW_AUTOSIZE);

    img1 = imread("../hof.jpeg");

    cvtColor(img1,img2,COLOR_RGB2GRAY);
    Canny(img2,img3,10,100,3,true);

    imshow("W1",img1);
    imshow("W2",img2);
    imshow("W3",img3);
*/
    waitKey(0);

    return 0;
}
