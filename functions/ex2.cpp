#include<opencv2/opencv.hpp>
#include<iostream>

using namespace std;
using namespace cv;

int main(int arg, char**argv)
{


    Mat image = Mat::zeros(500,500,CV_8UC3);

    namedWindow("W1",CV_WINDOW_AUTOSIZE);

    Rect roi (Point(100,50),Point(400,200));

    Mat subImg = image(roi);

    //vertical drawning 
    for(auto i = 0;i<subImg.rows;++i)
    {
        subImg.at<Vec3i>(i,0) = Vec3i(255,0,0);
        subImg.at<Vec3i>(i,subImg.cols) = Vec3i(255,0,0);
    }

    //horizontal drawning
    for(auto j = 0;j<subImg.cols;++j)
    {
        subImg.at<Vec3i>(0,j) = Vec3i(255,0,0);
        subImg.at<Vec3i>(subImg.rows,j) = Vec3i(255,0,0);
    }
    
    imshow("W1",image);
    waitKey(0);
    return 0;
}