#include<opencv2/opencv.hpp>
#include<iostream>

using namespace std;
using namespace cv;

int main(int arg, char**argv)
{


    Mat image = Mat::zeros(100,100,CV_8UC3);

    namedWindow("W1",CV_WINDOW_AUTOSIZE);

    circle(image,Point(50,50),10,Scalar(255,255,255),1);
    
    imshow("W1",image);
    waitKey(0);
    return 0;
}